import { Storage } from "@depixy/storage";
import { Readable } from "stream";
import { Client, ClientOptions } from "minio";

export interface S3Option extends ClientOptions {
  bucket: string;
}

export class S3Storage implements Storage {
  protected client: Client;
  protected bucket: string;

  constructor(option: S3Option) {
    const { bucket, ...clientOption } = option;
    this.client = new Client(clientOption);
    this.bucket = bucket;
  }

  async save(
    key: string,
    stream: Readable,
    metadata?: Record<string, any>
  ): Promise<void> {
    await this.client.putObject(this.bucket, key, stream, metadata);
  }

  async load(key: string): Promise<Readable> {
    return this.client.getObject(this.bucket, key);
  }

  async exist(key: string): Promise<boolean> {
    try {
      await this.metadata(key);
      return true;
    } catch (e) {
      return false;
    }
  }

  async metadata(key: string): Promise<Record<string, any>> {
    const stat = await this.client.statObject(this.bucket, key);
    return {
      ...stat.metaData,
      etag: stat.etag,
      "last-modified": stat.lastModified
    };
  }

  async delete(key: string): Promise<void> {
    await this.client.removeObject(this.bucket, key);
  }

  async deleteMany(keys: string[]): Promise<void> {
    await this.client.removeObjects(this.bucket, keys);
  }
}

export default S3Storage;
