# [1.3.0](https://gitlab.com/depixy/storage-s3/compare/v1.2.0...v1.3.0) (2020-07-23)


### Features

* add delete ([5ac2f77](https://gitlab.com/depixy/storage-s3/commit/5ac2f7755854a027e269a4b23ec93b5a5895d52d))

# [1.2.0](https://gitlab.com/depixy/storage-s3/compare/v1.1.0...v1.2.0) (2020-07-21)


### Features

* support etag, lastModified in metadata ([6e4215a](https://gitlab.com/depixy/storage-s3/commit/6e4215a6887afa0e97fe38791b2fffdfe20fe773))

# [1.1.0](https://gitlab.com/depixy/storage-s3/compare/v1.0.1...v1.1.0) (2020-07-21)


### Features

* add metadata support ([60e72b9](https://gitlab.com/depixy/storage-s3/commit/60e72b9bffe2c33ffbb87c69a5681d50493138e0))

## [1.0.1](https://gitlab.com/depixy/storage-s3/compare/v1.0.0...v1.0.1) (2020-07-21)


### Bug Fixes

* move @types/minio to dependencies ([280dbe0](https://gitlab.com/depixy/storage-s3/commit/280dbe0990148f0149cbc6ad4926ca1577e9de35))

# 1.0.0 (2020-07-20)


### Features

* initial commit ([82524c3](https://gitlab.com/depixy/storage-s3/commit/82524c33b317611c4e21fe9b2bbab69c753d4f38))
